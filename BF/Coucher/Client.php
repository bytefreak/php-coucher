<?php
namespace BF\Coucher;

class Client
{
	/**
	 * @var \BF\Coucher\Connection
	 */
	protected $connection;
	protected $selectedDB = null;

	public function setLogRequests($logRequests=true)
	{
		$this->connection->logRequests = $logRequests;
	}

	public function getLoggedRequests()
	{
		return $this->connection->loggedRequests;
	}

	public function __construct($baseurl)
	{
		$this->connection = new Connection($baseurl);
	}
	public function getRequestCount()
	{
		return $this->connection->requestsSent;
	}
	public function connect($username=null,$password=null)
	{
		$this->connection->connect($username,$password);
	}

	public function close()
	{
		$this->connection->close();
	}
	public function getSession()
	{
		$r = $this->connection->sendRequest(Connection::METHOD_GET,	"/_session");
		if (isset($r->body['error'])) throw new \Exception($r->body['error'].": ".$r->body['reason']);
		return $r->body;
	}
	public function getVersion()
	{
		$r = $this->connection->sendRequest(Connection::METHOD_GET, "/");
		if (isset($r->body['error'])) throw new \Exception($r->body['error'].": ".$r->body['reason']);
		return $r->body['version'];
	}

	public function getDatabases()
	{
		$r = $this->connection->sendRequest(Connection::METHOD_GET,"/_all_dbs");
		if (isset($r->body['error'])) throw new \Exception($r->body['error'].": ".$r->body['reason']);
		return $r->body;
	}
	public function getUUID()
	{
		$r = $this->connection->sendRequest(Connection::METHOD_GET,"/_uuids");
		if (isset($r->body['error'])) throw new \Exception($r->body['error'].": ".$r->body['reason']);
		return $r->body['uuids'][0];
	}

	public function getUUIDs($cnt)
	{
		$r = $this->connection->sendRequest(Connection::METHOD_GET,"/_uuids",array("count"=>$cnt));
		if (isset($r->body['error'])) throw new \Exception($r->body['error'].": ".$r->body['reason']);
		return $r->body['uuids'];
	}

	public function getStatus()
	{
		$r = $this->connection->sendRequest(Connection::METHOD_GET,"/_stats");
		if (isset($r->body['error'])) throw new \Exception($r->body['error'].": ".$r->body['reason']);
		return $r->body;
	}
	public function getConfig($section=null,$key=null)
	{
		$path = "/";
		if (!is_null($section)) {
			$path .= $section;
			if (!is_null($key)) $path .= '/'.$key;
		}

		$r = $this->connection->sendRequest(Connection::METHOD_GET,"/_config$path");
		if (isset($r->body['error'])) throw new \Exception($r->body['error'].": ".$r->body['reason']);
		return $r->body;
	}

	public function selectDatabase($db)
	{
		$this->selectedDB = $db;
	}

	public function getDatabaseInformations()
	{
		if (is_null($this->selectedDB)) throw new \Exception("no database selected!");
		$r = $this->connection->sendRequest(Connection::METHOD_GET,"/".$this->selectedDB);
		if (isset($r->body['error'])) throw new \Exception($r->body['error'].": ".$r->body['reason']);
		return $r->body;
	}

	public function createDatabase($name)
	{
		$r = $this->connection->sendRequest(Connection::METHOD_PUT, "/$name");
		if (isset($r->body['error'])) throw new \Exception($r->body['error'].": ".$r->body['reason']);
		return $r->body;
	}

	public function dropDatabase($name)
	{
		$r = $this->connection->sendRequest(Connection::METHOD_DELETE, "/$name");
		if (isset($r->body['error'])) throw new \Exception($r->body['error'].": ".$r->body['reason']);
		return $r->body;
	}

	public function getDocument($docId)
	{
		if (is_null($this->selectedDB)) throw new \Exception("no database selected!");
		$r = $this->connection->sendRequest(Connection::METHOD_GET, "/".$this->selectedDB."/".$docId);
		if (isset($r->body['error'])) return null;
		return $r->body;
	}

	public function bulkSaveDocuments(&$docs)
	{
		if (is_null($this->selectedDB)) throw new \Exception("no database selected!");
		$payload = json_encode(array("docs" => array_values($docs)));
		$r = $this->connection->sendRequest(Connection::METHOD_POST,"/".$this->selectedDB."/_bulk_docs",$payload);

		if (isset($r->body['error'])) throw new \Exception($r->body['error'].": ".$r->body['reason']);

		$keys = array_keys($docs);
		foreach ($r->body as $k => $docResponse) {
            if (is_object($docs[$keys[$k]])) {
                $docs[$keys[$k]]->_id = $docResponse["id"];
                $docs[$keys[$k]]->_rev = $docResponse["rev"];
            }else{
                $docs[$keys[$k]]["_id"] = $docResponse["id"];
                $docs[$keys[$k]]["_rev"] = $docResponse["rev"];
            }
		}

		return $r->body;
	}

	public function createDocument(&$document)
	{
		if (is_null($this->selectedDB)) throw new \Exception("no database selected!");
		$payload = $document;
		if (is_array($payload)) $payload = json_encode($payload);
		$r = $this->connection->sendRequest(Connection::METHOD_POST,"/".$this->selectedDB,$payload);

		if (isset($r->body['error'])) throw new \Exception($r->body['error'].": ".$r->body['reason']);

		$document["_id"] = $r->body['id'];
		$document["_rev"] = $r->body['rev'];

		return $r->body;
	}
	public function saveDocument($id,&$document)
	{
		if (is_null($this->selectedDB)) throw new \Exception("no database selected!");
		$payload = $document;
		if (is_array($payload) || is_object($payload)) $payload = json_encode($payload);
		$r = $this->connection->sendRequest(Connection::METHOD_PUT,"/".$this->selectedDB."/".$id,$payload);

		if (isset($r->body['error'])) throw new \Exception($r->body['error'].": ".$r->body['reason']);

		if (is_array($document)) {
			$document["_id"] = $r->body['id'];
			$document["_rev"] = $r->body['rev'];
		}elseif (is_object($document)){
			$document->_id = $r->body['id'];
			$document->_rev = $r->body['rev'];
		}
		return $r->body;
	}
	public function removeDocument($docOrId, $rev=null)
	{
		if (is_null($this->selectedDB)) throw new \Exception("no database selected!");
		if (is_object($docOrId)) $docOrId = (array) $docOrId;
		if (is_array($docOrId)) {
			$rev = $docOrId['_rev'];
			$docOrId = $docOrId["_id"];
		}
		if (is_null($rev)) throw new \Exception("Revision not provided as second parameter!");

		$r = $this->connection->sendRequest(Connection::METHOD_DELETE,'/'.$this->selectedDB.'/'.$docOrId, array("rev"=>$rev));
		if (isset($r->body['error'])) throw new \Exception($r->body['error'].": ".$r->body['reason']);

		return $r->body;
	}

	public function getAllDocuments($includeDocContent = true)
	{
		if (is_null($this->selectedDB)) throw new \Exception("no database selected!");

		return $this->performViewRequest('/'.$this->selectedDB.'/_all_docs',$includeDocContent);
	}

	public function getDocuments($keys,$includeDocContent=true,$options=array())
	{
		if (is_null($this->selectedDB)) throw new \Exception("no database selected!");
		$path = "/".$this->selectedDB."/_all_docs";
		$view = new View($this,$path,$keys,null,null,null,$includeDocContent,$options);

		return $view;
	}

	public function getView($designDoc,$viewname,$keys=null,$startkey=null,$endkey=null,$limit=null,$includeDocContent = true,$options = array())
	{
		if (is_null($this->selectedDB)) throw new \Exception("no database selected!");
		$path = "/".$this->selectedDB."/_design/$designDoc/_view/$viewname";

		$view = new View($this,$path,$keys,$startkey,$endkey,$limit,$includeDocContent,$options);

		return $view;
	}

	public function performViewRequest($path,$keys=null,$startkey=null,$endkey=null,$limit=null,$includeDocContent = true, $options = array())
	{
		$params = array();
		$params["include_docs"] = ($includeDocContent ? 'true' : 'false');

		if (!is_null($keys)) {
			if (!is_array($keys)) throw new \Exception("keys is kein array!");
			$params["keys"] = $keys;
		}
		if (!is_null($startkey)) $params["startkey"] = rawurlencode(json_encode($startkey));
		if (!is_null($endkey)) $params['endkey'] = rawurlencode(json_encode($endkey));
		if (!is_null($limit))  $params ['limit'] = (int)$limit;
		if (isset($options["startkey_docid"])) $params["startkey_docid"] = rawurlencode($options["startkey_docid"]);
		unset($options["startkey_docid"]);

		foreach ($options as $k => $v) {
			$params[$k] = rawurlencode(json_encode($v));
		}
		$keyData = array();
		if (isset($params["keys"])) {
			$keyData["keys"] = $params["keys"];
			unset($params["keys"]);
		}

		$qs = array();
		foreach ($params as $k => $v) {
			if (!is_null($v)) {
				$qs[] = "$k=$v";
			}
		}
		$qs = implode("&",$qs);

		$path .= '?'.$qs;

		$r = $this->connection->sendRequest(count($keyData) ? Connection::METHOD_POST : Connection::METHOD_GET,$path,count($keyData) ? json_encode($keyData) : array(),"application/json");
		if (isset($r->body['error'])) throw new \Exception($r->body['error'].": ".$r->body['reason']);

		return $r->body;
	}

	public function addAttachment($document,$name,$mimetype,$data)
	{
		if (is_null($this->selectedDB)) throw new \Exception("no database selected!");
		if (is_array($document)) $document = (object) $document;
		$docId = $document->_id;
		$docRev = $document->_rev;
		$path = "/".$this->selectedDB."/$docId/$name?rev=$docRev";
		$r = $this->connection->sendRequest(Connection::METHOD_PUT, $path,$data,$mimetype,array("Content-Length"=>strlen($data)));

		if (isset($r->body['error'])) throw new \Exception($r->body['error'].": ".$r->body['reason']);

		return $r->body;
	}
	
	public function callUpdateHandler($designDoc,$handler,$params,$docOrDocId=null)
	{
		if (is_null($this->selectedDB)) throw new \Exception("no database selected!");
		$path = "/".$this->selectedDB."/_design/".$designDoc."/_update/".$handler;
		
		$method = null;
		if ($docOrDocId) {
			$method = Connection::METHOD_PUT;
			if (is_array($docOrDocId)) $docOrDocId = (object)$docOrDocId;
			$docId = (is_object($docOrDocId)) ? $docOrDocId->_id : $docOrDocId;
			$path .= "/$docId";	
		}else{			
			$method = Connection::METHOD_POST;
		}
		$r = $this->connection->sendRequest(Connection::METHOD_POST, $path, $params);
		if (isset($r->body['error'])) throw new \Exception($r->body['error'].": ".$r->body['reason']);
		return $r->body;
	}
}