<?php
namespace BF\Coucher;
/*
 <?php
	$opts = new \BF\Coucher\PageableViewOptions("designDoc","viewname", 20);
	$opts->includeDocuments = true;
	$page = \BF\Coucher\Page::query($couchdbClient, $opts);

	foreach ($page->rows as $row) {
		....
	}

	$pager = $page->getPager();
 ?>

 <?php if ($pager["offsetStart"]>0) { ?>
	<a href="<?php echo "?$pager[prevQueryString]"; ?>">Vorherige Seite</a>
 <?php } ?>

 <?php echo $pager["offsetStart"]+1; ?>-<?php echo $pager["offsetEnd"]+1; ?> von <?php echo $pager["total"]; ?>

 <?php if ($pager["hasMore"]) { ?>
	<a href="<?php "?$pager[nextQueryString]"; ?>">N�chste Seite</a>
 <?php } ?>
 */
class Page
{
	/**
	 * @var PageableViewOptions
	 */
	public $options;

	public $count = 0;
	public $rows = array();
	public $totalCount = 0;
	public $hasMore = false;
	public $nextKey = null;
	public $nextDocId = null;

	public function __construct(PageableViewOptions $options, View $view)
	{
		$this->options = $options;

		$rows = $view->getRows();
		if ($options->direction==PageableViewOptions::DIRECTION_BACKWARD) {
			$rows = array_reverse($rows);
			if (count($rows)) {
				$this->options->startKey = $rows[0]["key"];
				$this->options->startDocId = $rows[0]["id"];
			}else{
				$this->options->startKey = null;
				$this->options->startDocId = null;
			}
		}

		$this->rows = array_slice($rows, 0, $options->limit);
		$this->count = count($this->rows);
		$this->totalCount = $view->getTotalCount();
		$this->hasMore = $options->direction==PageableViewOptions::DIRECTION_FORWARD ? $view->getCount()>$this->count : true;

		if ($this->hasMore || $options->direction==PageableViewOptions::DIRECTION_BACKWARD) {
			// getting nextKeys params!
			$lastDoc = $rows[$options->limit];
			$this->nextKey = $lastDoc["key"];
			$this->nextDocId = $lastDoc["id"];
		}
	}

	public function getPager()
	{
		$data = array(
				"total" => $this->totalCount,
				"count" => $this->count,
				"offsetStart" => min(array( $this->count ? $this->options->offset : 0,$this->totalCount-1 )),
				"offsetEnd" => min(array($this->count ? $this->options->offset + $this->count - 1 : 0, $this->totalCount-1)),
				"offsetNext" => min(array($this->count ?  $this->options->offset + $this->count : 0, $this->totalCount-1)),
				"offsetPrev" => max(array(0,$this->options->offset - $this->options->limit)),
				"hasMore" => $this->hasMore,
				"nextKey" => $this->nextKey,
				"nextDocId" => $this->nextDocId
		);

		$data["nextQueryString"] = "offset=$data[offsetNext]";
		$data["prevQueryString"] = "offset=$data[offsetPrev]";

		if ($this->hasMore) {
			$data["nextQueryString"] .= "&nk=".rawurlencode(base64_encode(json_encode($data["nextKey"])))."&nd=".rawurlencode($data["nextDocId"]);
		}
		if ($this->options->offset>0) {
			$data["prevQueryString"] .= "&desc=1&nk=".rawurlencode(base64_encode(json_encode($this->options->startKey)))."&nd=".rawurlencode($this->options->startDocId);
		}

		return $data;
	}

	/**
	 * @return Pager
	 */
	public static function query(Client $couchdb, PageableViewOptions $options)
	{
		$opt = array_merge($options->additionalOptions,array(
				"descending" => $options->descending,
				"startkey_docid" => $options->startDocId
		));
		if (!is_null($options->skip)) $opt["skip"] = $options->skip;
		if ($options->direction==PageableViewOptions::DIRECTION_BACKWARD) {
			$opt["descending"] = !$opt["descending"];
		}

		$view = $couchdb->getView($options->designDocument, $options->viewName,null,$options->startKey,$options->endKey,$options->limit+1,$options->includeDocuments,$opt);
		$page = new Page($options,$view);
		return $page;
	}
}