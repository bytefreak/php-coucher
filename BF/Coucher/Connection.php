<?php
namespace BF\Coucher;

use Httpful\Response;
use Httpful\Request;

class Connection
{
	const METHOD_GET = "get";
	const METHOD_POST = "post";
	const METHOD_DELETE = "delete";
	const METHOD_PUT = "put";

	protected $baseurl;
	protected $cookiesString;
	public $requestsSent = 0;
	public $logRequests = false;
	public $loggedRequests = array();

	public function __construct($baseurl=null)
	{
		if (substr($baseurl, strlen($baseurl)-1)=="/") $baseurl = substr($baseurl, 0, strlen($baseurl)-1);
		$this->baseurl = $baseurl;
	}

	public function sendRequest($method,$path,$params=array(),$forced_mime=null,$headers = array())
	{
		$uri = $this->baseurl.$path;
		$qs = null;
		$mime =null;

		if (is_array($params) && count($params)>0) {
			$qs = "";
			foreach ($params as $k => $v) {
				$qs .= rawurlencode($k)."=".rawurlencode($v)."&";
			}
			$qs = rtrim($qs,'&');

			if ($method==self::METHOD_GET || $method==self::METHOD_DELETE) {
				if (substr_count($uri, "?")>0) {
					$uri .= '&'.$qs;
				}else{
					$uri .= '?'.$qs;
				}
				$qs = null;
			}
			$mime = "application/x-www-form-urlencoded";
		}elseif(!is_array($params) && $params) {
			$qs = $params;
			$mime = "application/json";
		}

		// autoparse=false, because we will json parse it itself as array
		$request = Request::$method($uri)->expectsJson()->autoParse(false);

		foreach ($headers as $k => $v) {
			$request->addHeader($k,$v);
		}

		if (strlen($this->cookiesString)>0) {
			$request->addHeader("Cookie",$this->cookiesString);
		}

		if ($qs) {
//			echo ($qs)."<br><hr>";
			$request->body($qs);
			$request->sends($mime);
		}

		if ($forced_mime) $request->sends($forced_mime);

		$timer = null;
		if (class_exists("\\BF\PhpUtils\\Timer")) $timer = new \BF\PhpUtils\Timer();
		$response = $request->send();

		$response->body = json_decode($response->raw_body,true);
		$this->requestsSent++;

		if ($this->logRequests) {
			$logRequest = array(
				"request" => array("uri" => $request->uri, "headers" => $request->headers),
				"response" => array("headers" => explode("\n",$response->raw_headers)),
			);
			if (isset($timer)) $logRequest["duration"] = $timer->splitTime(false);

			$this->loggedRequests[] = $logRequest;
		}

		return $response;
	}

	public function connect($username=null,$password=null)
	{
		if (!is_null($username) && !is_null($password)) {
			$r = self::sendRequest(self::METHOD_POST, "/_session",array("name"=>$username,"password"=>$password));
			if (!isset($r->body['ok']) || !$r->body['ok']) {
				throw new \Exception("Login failed!");
			}

			$cookies = array();
			$headers = explode("\n",$r->raw_headers);
			foreach ($headers as $header) {
				$tmp = explode(": ",$header,2);
				if (strtolower($tmp[0])=="set-cookie") {
					$val = explode(";",$tmp[1]);
					$val = $val[0];
					list($name,$value) = explode("=",$val);
					$cookies[$name] = $value;
				}
			}
			$this->cookiesString = "";
			$tmp = array();
			foreach ($cookies as $k => $v) {
				$tmp[] = rawurlencode($k)."=".rawurlencode($v);
			}
			$this->cookiesString = implode(";",$tmp);
		}
	}

	public function close()
	{
		$this->sendRequest(self::METHOD_DELETE, "/_session");
		$this->cookiesString = null;
	}
}