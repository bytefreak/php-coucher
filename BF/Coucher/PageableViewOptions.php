<?php
namespace BF\Coucher;

class PageableViewOptions
{
	const DIRECTION_FORWARD = "f";
	const DIRECTION_BACKWARD = "b";

	public $designDocument;
	public $viewName;
	public $startKey = null;
	public $endKey = null;
	public $startDocId = null;
	public $descending = false;
	public $limit = null;
	public $includeDocuments = false;
	public $offset = 0;
	public $skip = null;
	public $additionalOptions = array();

	public $direction = self::DIRECTION_FORWARD;

	public function __construct($designDoc,$viewname,$limit,$autoFillFromRequest=true)
	{
		$this->designDocument = $designDoc;
		$this->viewName = $viewname;
		$this->limit = $limit;
		if ($autoFillFromRequest) {
			$this->offset = isset($_REQUEST["offset"]) ? $_REQUEST["offset"] : 0;
			$this->startKey = isset($_REQUEST["nk"]) ? json_decode(base64_decode($_REQUEST["nk"]),true) : null;
			$this->startDocId = isset($_REQUEST["nd"]) ? $_REQUEST["nd"] : null;
			$this->direction = isset($_REQUEST["desc"]) && $_REQUEST["desc"] ? self::DIRECTION_BACKWARD : self::DIRECTION_FORWARD;
		}
	}
}