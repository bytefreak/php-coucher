<?php
namespace BF\Coucher;

class View 
{
	/**
	 * @var \BF\Coucher\Client
	 */
	protected $client;
	protected $path;
	protected $includeDocContent;
	protected $result;
	protected $keys;
	protected $startkey;
	protected $endkey;
	protected $limit;
	protected $options = array();
	
	public function __construct(Client $client,$path,$keys,$startkey,$endkey,$limit,$includeDocContent,$options=array())
	{
		$this->client = $client;
		$this->path = $path;
		$this->includeDocContent = $includeDocContent;	
		$this->keys = $keys;
		$this->startkey = $startkey;
		$this->endkey = $endkey;
		$this->limit = $limit;
		$this->options = $options;
		
		$this->fetchData();
	}
	
	protected function fetchData()
	{
		$this->result = $this->client->performViewRequest($this->path,$this->keys,$this->startkey,$this->endkey,$this->limit,$this->includeDocContent,$this->options);
	}
	
	public function getCount() { return count($this->result['rows']); }
	public function getTotalCount() { return $this->result['total_rows']; }
	public function getRows() { return $this->result['rows']; }
	public function getFirst() { return $this->getCount()==0 ? null : $this->result['rows'][0]; }	
}
